import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Quiz from './Containers/Quiz';
import * as serviceWorker from './serviceWorker';
import questionReducer, { initialState } from './Stores/Quiz/reducer';
import { createStore } from 'redux';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux';


const store = createStore(questionReducer, initialState);

const routing = (
  <Router>
    <div>
      <Route exact path="/" component={App} />

      <Route path="/quiz">
        <Provider store={store}>
                <Quiz/>
        </Provider>)
        </Route>
    </div>
  </Router>
)

ReactDOM.render(
  routing,
  document.getElementById('root')
);

serviceWorker.unregister();
