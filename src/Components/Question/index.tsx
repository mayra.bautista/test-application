import React, { useEffect, useState } from 'react'
import IQuestion from '../../Models/question'
import IQuestionOption from '../../Models/questionOption';
import './styles.css';
import { Checkbox, FormControlLabel } from '@material-ui/core';


function Question({number,question, handleOptions}:IProps){

    const [indexArray,setIndexArray ]=useState([] as number[]);

    useEffect(()=>{
         const x=[] 
         for (var i = 0; i <= question.options.length-1; i++) {
             x.push(i);
         }
           shuffleArray(x);
           setIndexArray(x);
     }, []);
 
     function shuffleArray(array:any) {
         for (var i = array.length - 1; i > 0; i--) {
             var j = Math.floor(Math.random() * (i + 1));
             var temp = array[i];
             array[i] = array[j];
             array[j] = temp;
         }
     }

    function handleChange(item:IQuestionOption){  
        const index = question.options.findIndex(element => element.id === item.id); 
        const newOptions= [...question.options];
        newOptions[index].selected=!newOptions[index].selected;
        handleOptions({...question,options:newOptions});
    }
return (
<section className="questionCmpt">
<div key={question.id}  > {number} {question.text}</div>
    {indexArray.map((number, index) => (
        <div key={question.options[number].id}>
            <FormControlLabel  control={
            <Checkbox
            checked={question.options[number].selected }
            onChange={() =>handleChange(question.options[number])}
            name={question.options[number].id}
            color="primary"
            />
        } label={question.options[number].text}/>  
        </div>
              
      ))}
  
</section>
)
}

Question.defaultProps= {
number:4,
question:{id:'',
    text:'',
    options:[{id :'',text: '', selected: false}]}
}

interface IProps{
    number:Number,
    question:IQuestion,
    handleOptions:any;
}

export default Question