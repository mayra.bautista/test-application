// Actions what support my reducer
import actions from './actions';
import IAction from '../../Models/action';
import fakeQuestions from './fake';
import IQuestion from '../../Models/question';

export const initialState = {
  questions: fakeQuestions
};



function updateState(state: IState, newProps: IState) {
  return { ...state, ...newProps };
}
function updateQuestion(state: IState, question: IQuestion) {
    
    const index = state.questions.findIndex(element => element.id === question.id);     
    const newQuestions=[...state.questions];
    newQuestions[index]=question;
    return { ...state, questions: newQuestions };
}

export default function reducer(state: IState = initialState, action: IAction) {
  const { payload } = action;
  switch (action.type) {
    case actions.UPDATE_STATE(payload).type:
      return updateState(state, payload);
    case actions.UPDATE_QUESTION(payload).type:
      return updateQuestion(state, payload);
    default:
      return state;
  }
}
interface IState {
  questions: IQuestion[],
 
}