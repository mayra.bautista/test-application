import IQuestion from '../../Models/question';

const fakeQuestions: IQuestion[] = [  
    { id: 'question1', text: 'question text 1', options: [ { id: 'q1a', text: 'option a', selected: true } ] },
    { id: 'question2', text: 'question text 2', options: [ { id: 'q2a', text: 'option a', selected: true }, { id: 'q2b', text: 'option b', selected: false }, { id: 'q2c', text: 'option c', selected: false } ] },
    { id: 'question3', text: 'question text 3', options: [ { id: 'q3a', text: 'option a', selected: true }, { id: 'q3b', text: 'option b', selected: false }, { id: 'q3c', text: 'option c', selected: false }  ] },
    { id: 'question4', text: 'question text 4', options: [ { id: 'q4a', text: 'option a', selected: true }, { id: 'q4b', text: 'option b', selected: false }, { id: 'q4c', text: 'option c', selected: false }  ] },
    { id: 'question5', text: 'question text 5', options: [ { id: 'q5a', text: 'option a', selected: true }, { id: 'q5b', text: 'option b', selected: false }, { id: 'q5c', text: 'option c', selected: false }  ] }
] ;
export default fakeQuestions;