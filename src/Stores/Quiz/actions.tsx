import ActionCreator from '../../Helpers/ActionCreator';

const actionCreator = new ActionCreator('Home');

export default {
  UPDATE_STATE: (payload: any) => actionCreator.create('UPDATE_STATE', payload),
  GET_QUESTIONS: (payload?: any) => actionCreator.create('GET_QUESTIONS', payload),
  UPDATE_QUESTION: (payload?: any) => actionCreator.create('UPDATE_QUESTIONS', payload)
}