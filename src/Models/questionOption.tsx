export default interface IQuestionOption {
    id: string,
    text: string,
    selected: boolean
  }