import IQuestionOption from "./questionOption"

export default interface IQuestion {
    id: string,
    text:string,
    options:IQuestionOption[]
  }

