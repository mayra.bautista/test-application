import React, { useEffect, useState } from 'react'
import IQuestion from '../../Models/question'
import Question from '../../Components/Question'
import { connect } from "react-redux";
import actions from "../../Stores/Quiz/actions";
import { Button } from '@material-ui/core';
import { Redirect } from 'react-router-dom';

function Quiz({questions, updateQuestion }:IProps){
  const [redirect, setRedirect]=useState(false);
  function finishQuiz(){
    exportToJson(questions);
    localStorage.removeItem('username');
    setRedirect(true);  
    console.log('finished');

  }

   const [indexArray,setIndexArray ]=useState([] as number[]);

    useEffect(()=>{
          const x=[] 
          for (var i = 0; i <= questions.length-1; i++) {
              x.push(i);
          }
            shuffleArray(x);
            setIndexArray(x);
      }, []);

    function shuffleArray(array:any) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }
   
    function handleOptions(question:IQuestion){
        updateQuestion(question);
    } 
    
    
    function exportToJson(objectData: any) {
      let filename = "export.json";
      let contentType = "application/json;charset=utf-8;";
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        var blob = new Blob([decodeURIComponent(encodeURI(JSON.stringify(objectData)))], { type: contentType });
        navigator.msSaveOrOpenBlob(blob, filename);
      } else {
        var a = document.createElement('a');
        a.download = filename;
        a.href = 'data:' + contentType + ',' + encodeURIComponent(JSON.stringify(objectData));
        a.target = '_blank';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      }
    }
    if (redirect) {
      return <Redirect push to="/" />;
    }

    return (
        <section>
            <div>
            <h1>Welcome: {localStorage.getItem('username')} </h1>    
            <Button 
              variant="contained"
              color='primary'
              type="button"
              onClick={finishQuiz}>Finish</Button>

            </div>
                {indexArray.map((number, index) => (
                    <div key={questions[number].id}>
                        <Question number={index+1} question={questions[number]} handleOptions={handleOptions} ></Question>
                    </div>
                   
                ))}
        </section>
    )
}

interface IProps{
    questions:IQuestion[],
    updateQuestion:any;
}

function mapStateProps(state: any) {
    return {
      questions: state.questions
    };
  }

  function mapDispatchProps(dispatch: any) {
    return {
       updateQuestion: function (question:IQuestion) {
        dispatch(actions.UPDATE_QUESTION(question));
        },
      getQuestions: function () {
        dispatch(actions.GET_QUESTIONS())
      }
    };
  }

  
export default connect(mapStateProps, mapDispatchProps)(Quiz);