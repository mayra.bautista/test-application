import React, { MouseEvent, useState } from 'react';
import Form from '../../../../Components/Form';
import TextField from '@material-ui/core/TextField';
import IUser from "../../../../Models/user";
import './styles.css';

function UserForm({ onAccess } :IProps) {

  const [form, setForm] = useState({ username: '' } as IUser );
  
  function handleAccess(event: MouseEvent<HTMLButtonElement>) {
    onAccess(form);
  }

  const actions = [
    {
    onClick: handleAccess,
    text: 'Go ',
    primary: true
  }];

  return (
    <section className="userFormCmpt">
      <Form actions={actions} isValid={true}>
        <TextField
          onChange={(event:any) => setForm({...form, username: event.target.value })}
          placeholder="write a username..."
          value={form.username}/>
      </Form>
    </section>
  )
}

interface IProps {
  onAccess: (user: IUser) => void
}

export default UserForm;