import React, { useState } from 'react';
import UserForm  from './Components/UserForm'
import IUser from '../../Models/user';
import { Redirect } from 'react-router-dom';

function Home(){
    const [redirect, setRedirect]=useState(false);
    
function handleAccess(user:IUser){
    localStorage.setItem('username', user.username);
    setRedirect(true);   
}
if (redirect) {
    return <Redirect push to="/quiz" />;
  }
    return (
        <section>
            Wellcome to Quiz 
            <UserForm onAccess={ handleAccess }></UserForm>
        </section>

    )
}

export default Home;